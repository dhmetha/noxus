const fs = require('fs');
const MongoClient = require('mongodb').MongoClient

const url = 'mongodb://localhost:27017/Noxus';

MongoClient.connect(url, function(err, db) {
    if (err)
    {
        console.log('error while connecting to the DB');
        console.error(err);
        return;
    }
    console.log('Connected to MongoDB');

    fs.readdir('db', (err, files) => {
        if (err)
        {
            console.log('error while reading dir');
            console.error(err);
            return;
        }
        console.log('Dir readed');
        
        files.forEach((f) => {
            let name = f.split('.')[0];

            fs.readFile(`db/${f}`, 'utf8', (err, data) => {
                if (err)
                {
                    console.log('error while reading file');
                    console.error(err);
                    return;
                }
                if (f == 'areas.json') return;
                if (f == 'smileys.json') return;
                // if (f == 'items.json') return;

                console.log(`-- ${f} --`);
                console.log('File readed');

                let json = JSON.parse(data);
                
                console.log('Content parsed');

                console.log(`Starting insert => ${f}`);

                db.collection(name).insert(json).then(() => { console.log(`Inserted => ${f}`); });
            });
        });
    });
});